
= for i in [series]; do [commands]; done =

The for loop is a very powerful tool in programming. It allows you to do a
repeated behavior. In the case of shell scripting, we actually are using a
for-each loop, which iterates over each element of a series.

Let's say we wanted to output the name of each item in the current directory.
We could, of course, do this with "ls", but let's say we wanted to do it with
a for loop. We could do that like so:

for i in *; do echo $i; done

Let's break this apart piece-by-piece. The first part establishes the series
we will iterate over. In this case, it's *, which is a catch-all for
everything. In this case that means, every item in the current directory.
For i in * means that "i" will take on the value of each item in *, one-by-one.
When we want to reference the item that is currently i, we use the dollar sign:
$i to indicate that we're talking about a variable.

The second part is the command that we're doing with each i. We start this
section with "do". We could actually have multiple commands here. For example,
we could say "do echo $i; cat $i;" Each command should be separated from the
next item with a semicolon. In this case, we just have one command: echo the
item that is i. In this case, it will print out to the console (what "echo"
does) the name of each item in the list, which is what i here represents.

The final bit is "done" which establishes that the loop is over.


Choosing the list that i (you can name it other things, by the way) should come
from requires some practice. For example, you might want all the directories.
This can be accomplished with:

for i in */

because directories are separated by slashes, while files do not have any
items within them and do not need slashes.

If you wanted everything that ends with room, you could do *room. If you wanted
everything that begins with enemy, you could do enemy*. If you wanted
everything with an o somewhere in the name, you could do *o*. The asterisk is a
powerful tool.

