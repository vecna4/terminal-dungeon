#!/bin/sh

guess=$1

if [ "$1" == "azure" ]; then
echo ""
echo 'The trickster bows her head. "You'\
"'ve figured it out! "\
'Congratulations!"'
echo ""
echo 'She goes on to say, "The dragon can be found here." She writes a path.'
echo ""
echo "../../../../../.extras/lair_room/"
echo ""
else
echo ""
echo 'The trickster gives you a mischievous smirk. "If you want to get to the'
echo "dragon, you'll have to listen to my story and figure out the secret"
echo 'password. Then I will tell you where to find the dragon.'
echo ""
echo "As we are all well aware, dragons love to hoard wealth. This is a tale"
echo "about one such dragon."
echo ""
echo "The True Dragon Among Us"
echo ""
echo "Sir Asher plowed his way through the thick forest that preceded the great"
echo "mountain on the edge of the Kingdom of Þanðyrr. His armor shone brightly"
echo "in the sun, a blinding warning to any who dare oppose him. As he neared"
echo "the center of the forest, the sound of running water met his ears. A"
echo "river lay ahead of him, and the bridge over it was guarded by a troll."
echo "Asher was all too familiar with these trolls. They would ask a challenging"
echo "riddle to which he would never be able to find the answer, and Asher"
echo "would not be allowed to pass until he could come to that elusive"
echo 'solution. The knight despised such creatures. "Charge at full speed,'
echo 'Skenla!" he bellowed to his snow white horse who sped like lightning'
echo "toward the bridge."
echo ""
echo 'The troll had just begun to ask, "What gets bigger the more you take out'
echo 'of it?" when the sharp blade of Sir '\
"Asher's sword took his head clean"
echo "off, tainting the pure water below the bridge with dark crimson pools of"
echo "troll blood."
echo ""
echo "The horse and her rider bolted off, heading deeper into the forest. They"
echo "soon came upon a pair of pixie sisters, Ander and K, just reaching the"
echo 'end of a game of Qan. "Sir Asher!" cried the pixies. "O dear knight, we'
echo "have reached the end of Qan, but we are not very skilled at counting."
echo 'Please judge our game?"'
echo ""
echo "Sir Asher was quite fond of these pixies, and, always one to help those"
echo "in need, he halted his steed and went to examine the game. Ander was left"
echo "with her 1, 2, 3, 5, and 20-value pieces, while K retained her 15, 18,"
echo "and 19-value figures. Asher looked down at the board for a mere minute"
echo 'before declaring, "Clearly the winner is Ander! Certainly, a player who'
echo "maintains her 20-value piece must be the winner at the end of the game,"
echo "and furthermore, she finished the game with twice as many pieces as her"
echo 'opponent!"'
echo ""
echo "Ander grinned smugly upon hearing that she had won, but K seemed"
echo 'disheartened. "I tried so hard," she commented, quietly.'
echo ""
echo "Upon seeing her expression, the knight determined that he must comfort"
echo 'her. "Dear child," he said to her, "please do not be sad. '\
"'Twas but a "
echo 'single game. And," his voice fell dramatically, if my intuition is'
echo 'correct, you may well come out victorious in the long run." K’s face'
echo "brightened up after that, and her heart was set on overcoming her sister's"
echo "skill."
echo ""
echo "Pleased with himself for his skillful judging, Sir Asher whistled a tune"
echo "out-of-key as he again mounted Skenla and shot off to the mountain which"
echo "held the lair of the Great Green Dragon Þurrshgüll. He soon reached the"
echo "mountain's base where Þurrshgüll’s cave lay. "\
'"Hear ye, dragon!" shouted Sir'
echo 'Asher into the cave. "By order of the king, I, Sir Asher, the Brave Knight'
echo 'of the Dancing Flame, have come to collect your wealth!" Already the'
echo "knight was positioning Skenla, preparing her to rush into the lair. He"
echo "watched and listened intently but was unable to identify any movement or"
echo 'sound within the cave, save for his own voice’s echo. "Come out and face'
echo 'me, beast! If you do not deliver your treasure to me peacefully, I shall'
echo 'take it by force!"'
echo ""
echo "Sir Asher's voice continued to bounce off the cave walls for a moment"
echo 'before it was overcome by a deep, wearied rumble, "Leave me be, knight.'
echo 'I wish nothing to do with you or your kind."'
echo ""
echo 'The knight did not relent. "O dragon, hear me now! The king '\
"shan't accept"
echo 'your refusal kindly! Come out and face me now!"'
echo ""
echo 'The voice returned, "Leave me and my cave."'
echo ""
echo "Asher, never one to back down, charged full-speed into the cave. As the"
echo "walls raced past, a blur his eyes were not quick enough to capture, he"
echo "witnessed the gleaming of something shiny within the cave for the briefest"
echo "moment. The thought of the treasure he would find was his last thought"
echo "before his torso was enclosed by an enormous pair of jaws. The gargantuan"
echo "forest-green dragon's head pulled the knight's limp body upward, tossing"
echo "him in the air, spattering blood in a messy line along the wall. The"
echo "well-armored body fell – far more gracefully, indeed, than the knight had"
echo "ever been in life – into the mouth of the dragon and was swallowed whole."
echo ""
echo "The mighty dragon fixed her aged emerald eyes upon the horse before her."
echo 'She spoke again in her deep rumble. “You needn’t fear. I bear you no ill'
echo 'intent. Go. You are free now." Skenla did not need to be told twice, and'
echo "she scrambled out of the cave, galloping away as fast as her legs could"
echo "carry her."
echo ""
echo ""
echo ""
echo "When King Aerik saw the white horse galloping back to the castle, he"
echo "sighed sadly. Sir Asher was brave, but sometimes his bravery made him"
echo "foolish. He must have confronted the dragon and been killed. Aerik"
echo "greeted Skenla at the gates and gently patted her muzzle to comfort her."
echo '"Was Sir Asher killed by Þurrshgüll?" he asked the horse. She snorted in'
echo "a way that seemed to the king to mean yes. Aerik removed the saddle and"
echo 'reigns from Skenla and admired her in all her glory. "You have served your'
echo 'master well, and now you are free to live as you please," said the king'
echo "to the horse. Skenla bowed her head respectfully to the king; she appeared"
echo "to be mourning her former master. She then turned and trotted off to live"
echo "her life as a free horse."
echo ""
echo "Aerik looked out over the little kingdom of Þanðyrr. Þanðyrr was first"
echo "established by (and named after) his father, a proud king named Þann, but"
echo "he had died when Aerik was nine years old, and on his sixteenth birthday"
echo "when he came of age, the son took up the crown. King Aerik was a kind"
echo "young man, and he always wanted what was best for his kingdom. He wore a"
echo "modest crown and dressed in a purple robe that was not unnecessarily gaudy."
echo "Unlike his father, who would expend neither time nor resources to help"
echo "another person, Aerik spent his mornings listening to the plights of others"
echo "and his afternoons trying to remedy all of their crises."
echo ""
echo "That particular morning, a farmer named Lily had come into his court seeking"
echo "his aid. The river near the farms, she informed him, had overflowed and"
echo 'was flooding the kingdom’s crops. "My lord, we must build dykes along that'
echo 'river or we will not have any crops in the coming season!" she exclaimed.'
echo "Unfortunately, save for a large pile of boulders on the edge of the"
echo "kingdom that were far too heavy for the serfs in the kingdom to use and"
echo "far too hard for them to break down, there were no spare materials in"
echo "Þanðyrr that could be used to prevent the flood."
echo ""
echo "King Aerik had once been told by Princess Ororo of the nearby kingdom of"
echo "Elyaera that her domain had a surplus of smaller stones that they would"
echo 'be happy to sell to any of their neighbors. "That would work," his advisor,'
echo 'Lars Ulgrig, told him when Aerik suggested the idea, "but with our economy'
echo "still developing as it is, we could not possibly raise the taxes"
echo "necessary to pay for these materials. We can barely afford the programs"
echo "we are already employing with our taxes. There is one possible solution,"
echo 'but..."'
echo ""
echo '"But what?"'
echo ""
echo "Well, sir, the Great Green Dragon Þurrshgüll is technically a member of"
echo "our kingdom. She has amassed a great fortune over time; however, she has"
echo "a long-standing tradition of refusing to pay anything to the crown. If she"
echo "were to contribute, we could fix this problem in no time. I doubt you"
echo "would ever be able to convince her to work towards the good of the nation,"
echo 'though."'
echo ""
echo "Thus King Aerik had decided to send a representative to talk to Þurrshgüll."
echo "Sending Sir Asher had clearly been a mistake, but he was the only knight"
echo "brave enough to even approach Þurrshgüll’s lair, so King Aerik had"
echo "instructed him explicitly to be diplomatic and resolve the issue without"
echo 'violence. "Well," noted the king out loud, considering the loss of the knight,'
echo '"I suppose I should have known this would happen."'
echo ""
echo "Aerik considered his options. Someone else would need to talk to the great"
echo "creature. Ideally, mused the king, someone who would not have to face the"
echo "dragon's fury if the interaction did not go well. Suddenly, it came to him."
echo "He would ask Yllvår, the Blue Mage of the Northeast to contact Þurrshgüll"
echo "remotely. Yllvår was still young for a mage, less than a century old, and"
echo "while lacking the depth of experience possessed by many of the nearby"
echo "kingdoms' older, wiser wizards, Yllvår had a good heart. Aerik, always one"
echo "to think the best of others, truly believed that Þurrshgüll would hear"
echo "Yllvår's sincerity and listen. King Aerik set out immediately to beseech"
echo "the mage's aid."
echo ""
echo ""
echo ""
echo "Yllvår was reading an old tome, trying to memorize the spell therein when"
echo "they heard a knock on the door at the base of their tall tower. Looking"
echo "outside through their one-way window, they saw King Aerik standing upon"
echo "their doorstep. Yllvår quickly donned their nice robe (generally only worn"
echo "when company was around), a deep sapphire vestment that matched their"
echo 'gemlike eyes perfectly. "Coming!" Yllvår called in a light, musical voice.'
echo ""
echo '"Good morning, Miss... or Sire?" King Aerik hesitantly questioned. This'
echo "had always been a point of confusion to the monarch."
echo ""
echo "Yllvår laughed a soft, tinkling laugh and responded in a gentle voice,"
echo '"We mages have no use for such petty concepts of gender. But please, do'
echo 'come in, my good monarch! We can discuss that at length if you would like?"'
echo "The mage led the king up their spiraling staircase to a room at the top"
echo "of the tower filled with tomes whose titles were all in scripts King Aerik"
echo "was certain he had never before seen and arcane objects whose purposes"
echo "he could only guess."
echo ""
echo '"'\
"I'm afraid I haven't the time to discuss that today, Mage"\
' Yllvår."'
echo "Aerik was still not accustomed to dealing with Yllvår who seemed to eschew"
echo "all unnecessary formalities, and he was not sure how to address them."
echo '"Perhaps another day. For now, I have a favor to ask of you."'
echo ""
echo '"Oh?" asked the mage, guiding the king into their study. "And what might'
echo 'that be?"'
echo ""
echo '"Are you aware of the river’s recent overflow into the farmland?"'
echo ""
echo '"Yes," Yllvår replied solemnly, their voice dropping a full octave. "I was'
echo 'watching when it happened." They reached out a long, graceful arm and'
echo "pointed to the telescope positioned at the edge of the room, pointed"
echo "southwest toward the other end of the kingdom where the river’s edge lay."
echo '"I did – and have been doing what I can to hold the water at bay, but my'
echo 'powers are not yet nearly developed enough to cause much difference."'
echo ""
echo '"Thank you for what you have done. We appreciate every little bit of help."'
echo ""
echo 'Yllvår bowed slightly to the king. "I do what I can to benefit the kingdom."'
echo ""
echo '"Well," said the king hesitantly, "to that end... We have determined that'
echo "the only lasting solution would be for us to purchase stones to build dykes"
echo "along the river to ensure the water does not flood over. It seems the only"
echo "way we could accomplish this would be by requesting financial help from"
echo "Þurrshgüll, the great dragon. Unfortunately, she seems uninterested in"
echo "helping our community. She seems also to have killed the knight I sent out"
echo "to talk to her. I was hoping you could send her a magical message – remotely,"
echo 'to stay safe, of course." The king bowed his head humbly in earnest request.'
echo ""
echo "<Aerik, child of Þann...> A deep voice seemed to be speaking inside King"
echo "Aerik's head. He whipped his head around for a few seconds before realizing"
echo "it was a spell cast by Yllvår. <You are truly good. Because you always aim"
echo "to help your citizens, I too will attempt to help.> Yllvår went to fetch"
echo "an azure orb. As the mage's spindly fingers traced over the smooth"
echo "exterior of the orb, the blue color inside, which appeared to be a liquid"
echo "of sorts with a very low viscosity, swirled, mixing with something smoky"
echo 'and white. "With this," Yllvår said aloud, "I will contact the dragon and'
echo 'seek her aid." Aerik watched intently as Yllvår’s index finger traced'
echo 'counterclockwise around the circumference of the large crystal ball. Their'
echo "stare intensified; they seemed to be looking deeply through the sphere"
echo "into something beyond."
echo ""
echo "Yllvår’s mind entered the orb, and within a few moments, they were one - the"
echo "mage and the orb, united in their mental bond. They were lost in a sea of"
echo "auras, each with its own properties. They passed over Aerik's essence - white,"
echo "with traces of pink and the scent of cold, clean water. They felt the"
echo "essence of each creature within the kingdom, from the noble knights' sharp,"
echo "crimson auras which brought a taste of pickled green tomatoes to the mouth,"
echo "to the serfs' solid, sienna auras which were salty and sweet at the same"
echo "time and sounded like stones of many different sizes all rolling over each"
echo "other. Finally, they reached the powerful Þurrshgüll's aura. Yllvår had"
echo "felt the dragon's essence once before, when they first moved to the kingdom."
echo "At that time, it had been a vibrant emerald shade with a solid, cool,"
echo "pleasant texture. It had since become a faded jade, and it had solidified"
echo "even further but had lost its coolness. It now felt rough and grainy. It"
echo "smelled faintly of sulfur."
echo ""
echo "<O Great Green Dragon,> Yllvår's mind and the orb spoke in conjunction at"
echo "their deep pitch, <we humbly beseech your audience.>"
echo ""
echo "<Were you sent by the king?> came the reply. It sounded wearied, and Yllvår"
echo "and the orb felt it flow as if it contained an infinite number of"
echo "infinitesimally tiny grains of sand, all sifting around in a clockwise"
echo "circle."
echo ""
echo "<Yes, your greatness. We->"
echo ""
echo "<Tell the king I wish to be left alone,> Þurrshgüll interrupted."
echo ""
echo "<I will relay the message, but Great one->"
echo ""
echo "<Inform the king that I will speak to none other but him. Now leave my mind.>"
echo ""
echo "<Good dragon->"
echo ""
echo "Yllvår experienced a piercing sensation in their temple, and their"
echo "concentration slipped. Their mind exited the orb involuntarily, and they"
echo "cried out in pain. The orb slipped from their hand and shattered on the"
echo "floor. The blue substance that had been held inside it was now free of its"
echo "crystal captivity and had begun swirling, suspended in the air, forming"
echo "into the shapes of different plants, animals, and fungi."
echo ""
echo '"Are you alright?" asked the king immediately, with great concern in his voice.'
echo ""
echo "Yllvår panted for a few moments, holding their temple as the pain throbbed"
echo 'through their brain. "'\
"Dragons' mental faculties grow exponentially over time,"
echo "and Þurrshgüll has lived in this kingdom for a long time. She dealt a powerful"
echo 'psychic blow to me. I will be fine; please worry not for my sake, good king."'
echo 'Yllvår closed their eyes from the pain. "I may need to lie down for a bit'
echo 'though..." They headed to another room, leaving the shattered pieces of the orb'
echo 'on the floor, and gestured for the king to leave, foregoing their regular' 
echo 'politeness just a bit due to the pain. Before entering the room, they turned' 
echo 'back to the king. "Our dragon friend says that she wishes to be left alone and'
echo 'will not speak to anyone but to you."'
echo ""
echo '"Then," said the king to himself as he left their tower, his voice stoic'
echo 'despite his legs quivering from fear and nervousness, "I suppose, I must'
echo 'set out to visit our friend Þurrshgüll."'
echo ""
echo ""
echo ""
echo "Early the next morning, the king took an auburn horse from the stable and"
echo "headed off to Þurrshgüll's lair. King Aerik was not particularly sensitive"
echo "to magic, but even he could feel the powerful aura being exuded from within"
echo 'the dark cave. "Mighty dragon Þurrshgüll, I have come to speak with you,'
echo 'as you said you would only do with the king!"'
echo ""
echo "King Aerik felt the very walls of the cave shake as great, pounding footsteps"
echo "started moving toward him. He began to quiver, and he grasped the shaking"
echo "wall of the cave in a feeble attempt to steady himself. As Þurrshgüll grew"
echo "closer, King Aerik's limbs resembled the gelatinous slime creatures Sir"
echo "Asher (may he rest in peace) had once driven out of the kingdom. The stench"
echo "of burning leaves greeted his nose, and he coughed as the massive forest-"
echo "green-scaled head of Þurrshgüll came into sight. Þurrshgüll's emerald eyes,"
echo "as big as the Royal Knights' shields, set their gaze upon King Aerik,"
echo "then narrowed."
echo ""
echo '"You'\
"'re not King"\
' Þann," acknowledged Þurrshgüll in her deep rumble. When her'
echo "mouth opened to speak, Aerik saw her rows of sharp teeth, each one as long as"
echo "his thigh."
echo ""
echo '"Nay, Great Dragon, I am not. Þann was my father, and he has long since'
echo 'passed away. I am Aerik, the current king of Þannðyrr." With this, King'
echo 'Aerik bowed deeply to the dragon. "I implore your forgiveness for my tactless'
echo 'disregard; it seems that I did not introduce myself to you when I was'
echo 'coronated."'
echo ""
echo 'The dragon eyed the man quizzically. "Why have you come now to my lair?"'
echo ""
echo '"I must apologize, for this is again tactless of me, but I have come to'
echo 'ask for money."'
echo ""
echo "The dragon’s eyes narrowed."
echo ""
echo '"Please, Great one, hear my plea before you pass judgement!" urged the king,'
echo "who, caught in the piercing gaze of Þurrshgüll, feared greatly for his life."
echo '"You see, the rivers on the edge of the farmland in the kingdom of Þannðyrr'
echo 'have been overflowing. We wish to construct dykes along the edge of the'
echo 'river to prevent its overflow; however, we lack the materials to do so,'
echo 'and because our economy is still developing, we lack the gold to import the'
echo 'materials from other kingdoms. Without your help, Dragon Þurrshgüll, we'
echo 'feel that this task will be impossible, and we shall not have any food in'
echo 'the coming seasons."'
echo ""
echo "Þurrshgüll's eyes widened, and her face softened a bit."\
' "You came to ask'
echo 'me for taxes to help your citizens?"'
echo ""
echo '"Of course," responded Aerik weakly. "Any good king would do so. It is'
echo 'his duty!"'
echo ""
echo 'Þurrshgüll paused. "Perhaps you do not know your father as I did once."'
echo "She sighed a morose sigh which heated the air around Aerik uncomfortably,"
echo "but out of politeness he did not acknowledge it."
echo ""
echo '"Perhaps I do not. I was only nine when he passed away, and I had learned'
echo 'little of politics before then. I was not coronated until my sixteenth'
echo 'birthday, and the politics in this kingdom – what I was taught – could have'
echo 'drastically changed during those seven intercalary years." Thinking back,'
echo "it occurred to Aerik that during all of his training to be a fit ruler, he"
echo "had never been taught about his father's rule. The young boy had never asked,"
echo "and Lars, the one who governed the kingdom until Aerik came of age and"
echo "taught the king-to-be how to be a good monarch, must not have wanted him"
echo "to learn from his father's example."
echo ""
echo 'Þurrshgüll nodded, solemnly. "When I determined to take up residence in this'
echo "kingdom, it was because I believed it could be a great community. King Þann"
echo "was a good man when he first established the kingdom; however, he was soon"
echo "corrupted by the power of the crown. For the first few years, I tried to"
echo "work toward making this kingdom great. I gave taxes to the king, and I tried"
echo "to lend my services whenever anything was asked of me. The citizens of the"
echo 'kingdom were quite fond of me, as I recall." She laughed lightly, and her'
echo "emerald eyes twinkled for a moment before a mournful countenance set over"
echo 'her. "Well, it was not long before I realized the kind of king Þann was'
echo 'becoming. He stopped caring for his citizens and his kingdom."'
echo ""
echo "Þurrshgüll looked into Aerik's eyes, and he could sense that she was looking"
echo 'into his soul. "King Aerik, your heart still holds its vitality, and that'
echo "is what makes you good. Do you know what happens to your heart as you get"
echo 'older?"'
echo ""
echo "King Aerik, entranced by her speech, shook his head."
echo ""
echo '"As it ages, your heart naturally grows weaker. Unable to stand as it is,'
echo "it must resort to one of two paths. Your father's heart died, and in its"
echo "place, his greed took over. He stopped listening to the needs of others;"
echo "he refused to share his wealth; he would tax his citizens at constantly"
echo "increasing rates, merely for the sake of increasing his own affluence. He"
echo "then spent that treasure frivolously on unnecessarily fine clothing,"
echo "increasing the size of the castle, enjoying bountiful feasts imported from"
echo "nearby kingdoms while his citizens starved..."
echo ""
echo '"You said that this'\
" kingdom's economy is still developing. The truth of the"
echo "matter is that it is not developing for the first time; it is still trying to"
echo "recover from the damage your father caused to it. He mined all the gold"
echo "he could get his greedy hands on and squandered all the resources in this"
echo "kingdom, and now you are left without even food to feed yourselves. I"
echo "decided early on that I could not support such a regime, and I stopped"
echo 'providing any aid to the king who would only misuse it."'
echo ""
echo "Þurrshgüll sighed a deep sigh, and her wise eyes gazed wistfully into the"
echo 'distance. "Well, that is what happens when your heart dies... My heart took'
echo "the other path; it hardened, and I became numb to the world. I stopped"
echo "feeling, and I secluded myself in this cave where my heart turned fully to"
echo "stone. Here I am now, a shell of my old self. Would you believe my scales"
echo 'were once as bright as my eyes?" She eyed her darkened scales that lacked'
echo 'the luster her youth had once lent her. "I tell you truthfully, I seem to'
echo "have aged one thousand years in the last one hundred. When I refused to"
echo "waste my possessions on him, your father called me 'dragon' as a derogatory"
echo "term. 'You greedy dragon!' he said. But I was not greedy; I just knew giving"
echo "to him was the wrong choice. The true 'dragon' among us, Aerik, was your"
echo "father."
echo ""
echo '"Regardless, I decided if that was how he felt about me anyway - that I'
echo "merely accumulate wealth and never share it - then I may as well do so and"
echo "show him how much of a 'dragon' I could be."\
'" Þurrshgüll growled gently.'
echo 'She then bent her head down. "I amassed a great fortune out of spite, but'
echo "it did me no favors. I miss how it was before. When your father cared."
echo "When I still had my vigor and vitality. When there was hope for this"
echo "kingdom..."
echo ""
echo '"Tell me, King," her eyes, intense, but benevolent, fixed themselves upon'
echo 'Aerik again. "What is done with the taxes you collect?"'
echo ""
echo 'Aerik cleared his throat. "Well, taxes are a very important part of the'
echo 'workings of this community. The collected taxes allow Nadia the Healer to'
echo 'provide free wound and poison curing for all. If you had children, they'
echo 'would be allowed to learn from Salzor and Ðöa, the Scholars whom we employ'
echo "to teach anyone who wishes to learn. We support our farmers with taxes,"
echo "and thus the food they harvest is widely available to all. We also use"
echo 'these taxes for special needs like this river situation."'
echo ""
echo "As the king continued, describing the plenitude of services the Castle"
echo "supported through taxation, Þurrshgüll's eyes widened in wonderment. Then"
echo 'they narrowed again. "What of war? Do your taxes fund a military? I seem'
echo 'to remember eating a knight earlier."'
echo ""
echo "Aerik’s gaze lowered. While he was a strong proponent of peace, it was"
echo "true that sometimes war was necessary, and certainly the kingdom could not"
echo 'exist without any sort of military. "Yes, we do have knights. And I do wish'
echo "to apologize thoroughly for the rash actions of my knight, Sir Asher. I"
echo "ordered him to come and talk peacefully with you as I am now, but he can..."
echo "well, he could be rather hot-headed. Our knights exist to protect our"
echo "citizens. We never try to invade our neighbors, but of course we must"
echo "defend the kingdom if we are attacked. During times of peace, our knights"
echo "make sure everyone is safe on a smaller scale and try to help settle"
echo "disputes. I always stress that they avoid violence unless it is completely"
echo 'unavoidable."'
echo ""
echo 'The dragon Þurrshgüll seemed satisfied with this answer: "I suppose that'
echo 'is about the best way to handle that," and she was certainly impressed'
echo 'with the social programs King Aerik had implemented in his kingdom: "You'
echo 'use your tax money to do all of those things for your citizens?"'
echo ""
echo 'King Aerik nodded again, repeating, "Of course. Wouldn’t any good king?"'
echo ""
echo ""
echo ""
echo "It is said that on that day, Þurrshgüll the Great Green Dragon felt her"
echo "stone heart soften entirely. In fact, that very afternoon, she decided to"
echo "exit her cave and live again. She spread her vast wings for the first time"
echo "in decades and took to the sky. The citizens of Þannðyrr were shocked to"
echo "hear her deep rumble of a voice laughing childishly and she soared through"
echo "the sky (perhaps not as majestically as a dragon in the practice of flying,"
echo "but they agreed that the great creature was majestic nonetheless). It"
echo "felt like a breath of new life, she reported later, to fly again, to feel"
echo "the wind beating against her body."
echo ""
echo "After her initial flight, her first order of business was to fix the"
echo "flooding river (which she had glimpsed during her flight). She had no"
echo "trouble collecting the boulders at the edge of the kingdom in her roc-egg-"
echo "sized arms, and with her powerful wings which spanned the length of a small"
echo "basilisk, she easily carried it over to the river’s edge and built up dykes"
echo "so tall and solid that the river could not possibly exceed them even if it"
echo "were given ten years of rain to rise or ten thousand years to erode them."
echo '"There," she proudly proclaimed, dusting off her claws, "now you '\
"needn't"
echo 'rely upon other kingdoms."'
echo ""
echo "Yllvår, resting in their tower, felt their brain throb when the dragon left"
echo "her cave, and when they looked through their telescope, they saw the dragon"
echo "building the great dykes. Yllvår rushed out of their tower and ran barefoot"
echo "(for they owned neither sandals nor boots) all the way to the river's edge"
echo 'to meet her. "Þurrshgüll!" they called. "Þurrshgüll, I implore your'
echo 'forgiveness for my rude intrusion into your mind!"'
echo ""
echo "Þurrshgüll immediately sensed the aura that could have belonged to none"
echo 'but that mage, and she turned to them. "Dear child," said the dragon'
echo 'gently, "I too must apologize. I am truly sorry for attacking you. I did'
echo "not understand the nature of the situation, and I should have allowed you"
echo 'to explain instead of hurting you." With that, the mighty Green Dragon'
echo "blinked slowly, and the pain from Yllvår’s migrain softened and then"
echo "dissolved."
echo ""
echo 'Yllvår bowed deeply to the dragon in thanks. "You are truly a powerful'
echo 'being, O Great Green Dragon."'
echo ""
echo "Þurrshgüll slowly examined the mage; she seemed to be peering into their"
echo "soul. She maintained this stare, contemplating the mage in front of her"
echo "for a full ten minutes after Yllvår rose from their bow. Finally, she"
echo 'spoke, "Blue Mage of the Northeast, you have much potential, but still'
echo 'there is much for you to learn. Your soul is pure, and for that reason, I'
echo 'have decided to extend to you an offer..."'
echo ""
echo "The exact nature of that offer remains a secret to this day, but it is"
echo "said that since then, Yllvår’s magic has greatly increased in power, and"
echo "onlookers began to notice that when Yllvår cast spells, sometimes their"
echo "eyes stayed their sapphire hue, but sometimes they turned a deep emerald."
echo "Yllvår, who still lives in Þannðyrr to this day, has since become a great"
echo "mage of incredible power, and some who are particularly perceptive would"
echo "swear that sometimes – just sometimes – their eyes are neither sapphire"
echo "nor emerald when they cast spells. Some claim that one eye will turn green"
echo "while one eye will stay blue; others claim the two colors mix, forming a"
echo "turquoise or seafoam or teal; the exact color varies from account to account."
echo ""
echo "When Lily, the Farmer later visited the site where the boulders had been,"
echo "she noticed that the newly exposed ground was filled with gold and other"
echo "precious metals. The king decreed that the treasure should belong to the"
echo "one who found it, but Lily insisted that the majority of it should go to"
echo "the betterment of the kingdom as a whole. (Of course, she kept a few large"
echo "blocks for herself. She had found it, after all.) Þurrshgüll with her newly-"
echo "softened heart donated the entirety of her treasure hoard (which notably"
echo "now included the regurgitated armor of the late Sir Asher) to the Castle's"
echo 'tax program. "Please," she insisted with a hearty laugh when the king'
echo 'questioned her to ensure she actually did not need it, "I have no use for'
echo 'treasure. I can fend for myself." With these treasures, '\
"the kingdom's"
echo "economic state vastly improved, and soon everyone in the kingdom was able"
echo "to live a comfortable and fulfilling life."
echo ""
echo "Þurrshgüll became a very well-known and well-liked public figure, and she"
echo "once again began helping anyone and everyone who requested a favor. She"
echo "would spend her days flying around, and anyone who needed help knew they"
echo "could flag her down. After only a few months of this, her scales had regained"
echo "their emerald hue, and she once again held her youthful vigor and joy."
echo ""
echo "The king became close friends with Þurrshgüll, and on his deathbed,"
echo "acknowledging Þurrshgüll's wisdom and warm heart, King Aerik (who had no"
echo "hereditary heir) passed the torch of ruler to her, declaring that for as"
echo "long as Þannðyrr stood, Þurrshgüll should be the one to offer guidance and"
echo "determine the programs implemented within the kingdom. Indeed, to this day,"
echo "Queen Þurrshgüll, though quite old, still does her very best to care for"
echo "every single member of her land, and she tells them stories of the kind"
echo "King Aerik, who will always be remembered as the greatest king Þannðyrr"
echo "has ever had."
echo ""
echo ""
echo ""
echo ""
echo ""
echo '"Thus ends my story," says the trickster. "Now I ask: What color was'
echo "Yllvår's orb?"\
'"'
fi
