
# Terminal Dungeon

## Quick Start

1. Download this project as a zip.
2. Unzip it.
3. Launch a terminal in the main folder: terminal-dungeon
4. Issue the following commands:
5. `./setup`
6. `cd game`
7. `cat info`


## Setup

It is recommended to download this game as a zip and keep the zip around, in
case you need to restore the game to its original state. The process of playing
the game involves modifying the source code.

Before you begin playing, run the `setup` script (in the main folder,
terminal-dungeon):

`./setup`


## How to play

Welcome, adventurer, to the Terminal Dungeon!

It is rumored that this dungeon contains great treasure, guarded by a
powerful dragon. Your task is to travel to the deepest cavern of the
dungeon and retrieve the treasure.

You are a special kind of spellcaster: a Terminal Sorcerer (a sourcerer,
if you will). You have immense power - the power to shape your very reality.
But you must first learn to use that power. Along this journey, you will
learn to harness your amazing potential.

To start the game, open a terminal in the "game" folder. You will begin in
front of the terminal dungeon. (You can also open the terminal here and type
"cd game".)

In each room, you will find a file titled info. This file will give you some
information about the room and possibly clues that will help you bypass
obstacles along the way. To read this file, use the spell:

`cat info`

You may have a better playing experience if your terminal uses different text
colors for different types of files. For example, if you can easily
differentiate between directories and files, and between executable files and
non-executables.

As you play, you will learn commands that can drastically alter the world of
the game and allow you to bend it to your will. There's a basic plot to the
game where you gradually exercise your powers in a restrained way over your
world.

Feel free to more drastically alter the game, bypass puzzles, etc. The goal is
to learn, and subverting the designed system is a great way to learn.

If you get stuck, most rooms have a file called "help" which can be read by
typing:

`cat help`

## Credits and Contact

This game was inspired by Andy Wallace's game terminaltown:
(https://github.com/andymasteroffish/terminal_town)

This game was created by Vecna. Feel free to visit
[vecna.xyz](https://vecna.xyz) for more stuff I do.
At this time, I can be contacted at vecna@disroot.org
([PGP key](https://vecna.xyz/files/misc/publickey.vecna@disroot.org.asc)). See
[vecna.xyz/contact](https://vecna.xyz/contact.html) for up-to-date contact
information.

This game is hosted on [GitLab](https://gitlab.com/vecna4/terminal-dungeon).
Issues can also be opened there.

## Licensing Information

This game is licensed under the GNU GPLv3 or later.

The text in the "scroll of ancient proverbs" is from Don Quixote and is in
the public domain.

All other text in this game (including the story The True Dragon Among Us) was
written by myself, Vecna, and is licensed under a Creative Commons Attribution
4.0 International (CC BY 4.0) license.

## Release Notes

v1.0
I hope you like this game. If anything doesn't work correctly, or if you have
feedback, please let me know!
